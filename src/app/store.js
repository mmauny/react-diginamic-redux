import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import formationReducer from '../features/formation/formationSlice';

export default configureStore({
  reducer: {
    counter: counterReducer,
    formations: formationReducer
  }
});
