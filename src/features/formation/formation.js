import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectFormations, ajouterFormation } from './formationSlice';

export function Formation() {
  const formations = useSelector(selectFormations);
  const dispatch = useDispatch();
  const [inputNom, setInputNom] = useState('')

  return (
    <div>
        <input value={inputNom} onChange={ (e) => setInputNom(e.target.value) }></input>
        <button onClick={() => dispatch(ajouterFormation({nom : inputNom}))}>Rajouter formation</button>
        {formations.map( (formation) => <div> {formation.nom}</div>)}
    </div>
  );
}
