import { createSlice } from '@reduxjs/toolkit';

export const formationSlice = createSlice({
  name: 'formations',
  initialState: {
    value: [ { nom : 'premiere formation'}],
  },
  reducers: {
    ajouterFormation: (state, action) => {
      state.value =  [...state.value, action.payload];
    }
  },
});

export const { ajouterFormation } = formationSlice.actions;

export const selectFormations = state => state.formations.value;

export default formationSlice.reducer;
