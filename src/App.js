import React from 'react';
import { Counter } from './features/counter/Counter';
import './App.css';
import { Formation } from './features/formation/formation';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Counter />
        <Formation></Formation>
      </header>
    </div>
  );
}

export default App;
